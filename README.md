# GUI4-youtube-dl
PyQt GUI for youtube-dl to download from youtube to mp3


FOR LINUX

You need to install PyQt5(should be installed),youtube-dl and avconv of ffmpeg before using gui4youtube-dl

FOR WINDOWS

You need to install Python3, PyQt5 using pip3 and download youtube-dl.exe and ffmpeg and put them in the same directory. The directory where you put gui4youtube-dl must have writing permission.

PS : .pyw extension is for no console when executing it on Windows.


